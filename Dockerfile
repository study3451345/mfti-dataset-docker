FROM basex/basexhttp:9.4.6

#USER 0
#RUN addgroup basex xfs
#USER 1984

RUN mkdir /srv/basex/webapp/simplex-frame
RUN git clone https://gitlab.com/kontur32/simplex-frame.git /srv/basex/webapp/simplex-frame

RUN mkdir /srv/basex/webapp/app
RUN git clone https://gitlab.com/study3451345/mfti-dataset.git /srv/basex/webapp/app

RUN mkdir -p /srv/basex/webapp/app/var/data

#RUN mkdir /srv/basex/webapp/static
#RUN mkdir /srv/basex/webapp/static/misis
#COPY ./static/misis /srv/basex/webapp/static/misis
